from collections import namedtuple
from random import randint

import optparse
import random
import sys

# Setting Command Line Arguments
optparser = optparse.OptionParser()
optparser.add_option("-s", "--seed", dest="seed", default=5, type="int", help="Numeric seed")
optparser.add_option("-w", "--width", dest="width", default=50, type="int", help="width")
optparser.add_option("-t", "--height", dest="height", default=60, type="int", help="height")
(opts, _) = optparser.parse_args()

seed = opts.seed
random.seed(seed)

location = namedtuple("location", "row, col")

SOLID_ID = 0
FLOOR_ID = 1
DOOR_ID = 2
ENTRANCE_ID = 3
CORRIDOR_ID = 4
HORIZANTAL_WALL_ID = 5
VERTICAL_WALL_ID = 6

SOLID = ' '
FLOOR = '#'
DOOR = 'D'
ENTRANCE = '*'
CORRIDOR = '.'
HORIZANTAL_WALL = '-'
VERTICAL_WALL = '|'

MAX_ROOM_AREA = 16
MAX_ROOM_WIDTH = 4
MAX_ROOM_HEIGHT = 4

class Room(object):
    def __init__(self, position, width, height):
        self.position = position
        self.width = width
        self.height = height
        self.door = None
        self.area = []
    
    @staticmethod
    def null_room():
      return Room(location(0, 0), 0, 0)
    
    def fill_area(self, position):
      self.area.append(position)
      
    def set_door(self, position):
      self.door = position            
    
    def __eq__(self, other):
      if isinstance(other, Room):
        return self.position.row == other.position.row and \
               self.position.col == other.position.col
      return NotImplemented
    
class Dungeon(object):
  
  def __init__(self, width, height, room_count):
    self.height = height
    self.width = width
    self.room_count = room_count
    self.dungeon = [[SOLID_ID for _ in xrange(width)] for _ in xrange(height)]
    self.rooms = []
    self.corridors = []
    
  def print_dungeon(self):
    for i in xrange(self.height):
      for j in xrange(self.width):
        if self.dungeon[i][j] == SOLID_ID:
          print SOLID, 
        elif self.dungeon[i][j] == FLOOR_ID:
          print FLOOR,
        elif self.dungeon[i][j] == ENTRANCE_ID:
          print ENTRANCE,
        elif self.dungeon[i][j] == CORRIDOR_ID:
          print CORRIDOR,
        elif self.dungeon[i][j] == HORIZANTAL_WALL_ID:
          print HORIZANTAL_WALL,
        elif self.dungeon[i][j] == VERTICAL_WALL_ID:
          print VERTICAL_WALL,
      print
      
  def add_first_room(self):
    dungeon_center_row = self.height / 2
    dungeon_center_col = self.width / 2
    room_position = location(dungeon_center_row, dungeon_center_col)
    room = self.create_room(room_position)
    for i in xrange(room_position.row, room_position.row + room.height):
      for j in xrange(room_position.col, room_position.col + room.width):
        self.dungeon[i][j] = FLOOR_ID
        room.fill_area(location(i, j))
    door_position = self.random_room_wall(room)
    self.dungeon[door_position.row][door_position.col] = DOOR_ID
    room.set_door(door_position)
    self.rooms.append(room)
  
  def add_room(self):
    random_position = self.random_location()
    new_room = self.create_room(random_position)
    if not self.rooms_collide(new_room):
      if self.has_space_for_room(new_room):
        for j in xrange(new_room.position.col, new_room.position.col + new_room.width):
          for i in xrange(new_room.position.row, new_room.position.row + new_room.height):
            self.dungeon[i][j] = FLOOR_ID
            new_room.fill_area(location(i, j))
        door_position = self.random_room_wall(new_room)
        self.dungeon[door_position.row][door_position.col] = DOOR_ID
        new_room.set_door(door_position)
        self.rooms.append(new_room)
        return True
    return False

  def rooms_collide(self, new_room):
    for room in self.rooms:
      if room != new_room:
        if room.position.row + room.height < new_room.position.row or \
             room.position.row > new_room.position.row + new_room.height or \
             room.position.col + room.width < new_room.position.col + 1 or \
             room.position.col > new_room.position.col + new_room.width:
          return False
    return True
  
  def has_space_for_room(self, room):
    for j in xrange(room.position.col, room.position.col + room.width):
      for i in xrange(room.position.row, room.position.row + room.height):
        if self.dungeon[i][j] != SOLID_ID or i == self.height - 1 or j == self.width - 1:
          return False
    return True
    
  def random_location(self):
    random_row = randint(MAX_ROOM_HEIGHT, self.height - MAX_ROOM_HEIGHT)
    random_col = randint(MAX_ROOM_WIDTH, self.width - MAX_ROOM_WIDTH)
    return location(random_row, random_col)
  
  def random_room_wall(self, room):
    rooms = len(room.area) - 1
    while True:
      random_index = randint(1, rooms)
      random_position = room.area[random_index]
      if self.is_empty_space_south(random_position):
        return location(random_position.row + 1,  random_position.col)
      
      if self.is_empty_space_west(random_position):
        return location(random_position.row,  random_position.col - 1)
      
      if self.is_empty_space_north(random_position):
        return location(random_position.row - 1,  random_position.col)
      
      if self.is_empty_space_east(random_position):
        return location(random_position.row,  random_position.col + 1)
  
  def is_empty_space_south(self, position):
    return position.row + 1 < self.height - 1 and self.dungeon[position.row + 1][position.col] == SOLID_ID
  
  def is_empty_space_west(self, position):
    return position.col - 1 > 0 and self.dungeon[position.row][position.col - 1] == SOLID_ID
  
  def is_empty_space_north(self, position):
    return position.row - 1 > 0 and self.dungeon[position.row - 1][position.col] == SOLID_ID
  
  def is_empty_space_east(self, position):
    return position.col + 1 < self.width - 1 and self.dungeon[position.row][position.col + 1] == SOLID_ID
  
  def create_room(self, position):
    width = randint(2, MAX_ROOM_WIDTH)
    height = randint(2, MAX_ROOM_HEIGHT)
    area = width * height
    while self.is_large_room(area) and not self.is_room_within_boundary(position):
      width = randint(2, MAX_ROOM_WIDTH)
      height = randint(2, MAX_ROOM_HEIGHT)
      area = width * height
    return Room(position, width, height)
  
  def is_room_within_boundary(self, position, room_width, room_height):
    return position.row + room_height > self.height or \
           position.col + room_width > self.width
  
  def is_large_room(self, room_area):
    return room_area > MAX_ROOM_AREA
    
  def add_corridors(self):
    for room in self.rooms:
      room2 = self.get_closest_room(room)
      if room2 == Room.null_room():
        continue
      room_door = room.door
      room2_door = room2.door
      
      while (room2_door.row != room_door.row) or (room2_door.col != room_door.col):
        if room2_door.row != room_door.row:
          if room2_door.row > room_door.row:
            room2_door = location(room2_door.row-1, room2_door.col)
          else:
            room2_door = location(room2_door.row+1, room2_door.col)
        elif room2_door.col != room_door.col:
          if room2_door.col > room_door.col:
            room2_door = location(room2_door.row, room2_door.col-1)
          else:
            room2_door = location(room2_door.row, room2_door.col+1)
        if self.is_connectable(room2_door) and self.is_within_boundary(room2_door):
          self.dungeon[room2_door.row][room2_door.col] = CORRIDOR_ID
          self.corridors.append(room2_door)
      
  def is_within_boundary(self, loc):
    return loc.row < self.height - 1 and loc.col < self.width - 1
    
  def is_connectable(self, loc):
    return self.dungeon[loc.row][loc.col] == SOLID_ID or self.dungeon[loc.row][loc.col] == DOOR_ID 
    
  def get_closest_room(self, room):
    closest_room = Room.null_room()
    closest_distance = sys.maxint
    for other_room in self.rooms:
      if room != other_room:
        distance = abs(room.door.row - other_room.door.row) + abs(room.door.col - other_room.door.col)
        if distance < closest_distance:
          closest_distance = distance
          closest_room = other_room
    return closest_room
  
  def build_dungeon(self):
    self.add_first_room()
    #self.print_dungeon()
    while len(self.rooms) < self.room_count:
      roomAdded = self.add_room()
      #self.print_dungeon()
      if roomAdded and len(self.rooms) > 1:
        self.add_corridors()
        #self.print_dungeon()
    self.add_walls()
    self.last_adjustments()
    self.add_entrance()
    self.print_legend()
    self.print_dungeon()
  
  def add_walls(self):
    for i in xrange(1, self.height - 1):
      for j in xrange(1, self.width - 1):
        if self.dungeon[i][j] == FLOOR_ID or self.dungeon[i][j] == CORRIDOR_ID:
          if self.dungeon[i+1][j] == SOLID_ID:
            self.dungeon[i+1][j] = HORIZANTAL_WALL_ID
          if self.dungeon[i-1][j] == SOLID_ID:
            self.dungeon[i-1][j] = HORIZANTAL_WALL_ID
          if self.dungeon[i][j+1] == SOLID_ID:
            self.dungeon[i][j+1] = VERTICAL_WALL_ID
          if self.dungeon[i][j-1] == SOLID_ID:
            self.dungeon[i][j-1] = VERTICAL_WALL_ID
  
  def last_adjustments(self):
    """ This function adds floors to rooms and corridors and closes some open spaces with walls """
    for i in xrange(self.height):
      for j in xrange(self.width):
        if self.dungeon[i][j] == FLOOR_ID:
          # CORRIDOR_ID will be shown eventually as the floor of the self.dungeon which includes rooms also
          self.dungeon[i][j] = CORRIDOR_ID
        if self.dungeon[i][j] == VERTICAL_WALL_ID:
          if i+1 < self.height:
            if self.dungeon[i+1][j] == SOLID_ID:
              self.dungeon[i+1][j] = HORIZANTAL_WALL_ID
          if i-1 > 0:
            if self.dungeon[i-1][j] == SOLID_ID:
              self.dungeon[i-1][j] = HORIZANTAL_WALL_ID
        
        if self.dungeon[i][j] == HORIZANTAL_WALL_ID:
          if i-1 > 0 and j-1 > 0:
            if self.dungeon[i-1][j] == CORRIDOR_ID and self.dungeon[i][j-1] == SOLID_ID:
              self.dungeon[i][j-1] = VERTICAL_WALL_ID
              
  def add_entrance(self):
    for i in xrange(self.height - 1, -1, -1):
      for j in xrange(self.width - 1, -1, -1):
        if self.dungeon[i][j] == HORIZANTAL_WALL_ID and i - 1 > 0:
          if self.dungeon[i - 1][j] != VERTICAL_WALL_ID:
            self.dungeon[i][j] = ENTRANCE_ID
            return
      
  def print_legend(self):
    print "%s: Horizontal Wall" % HORIZANTAL_WALL
    print "%s: Vertical Wall" % VERTICAL_WALL
    print "%s: Floor - includes corridors and rooms" % CORRIDOR
    print "%s: Entrance/Exit" % ENTRANCE
    print
    
if __name__ == '__main__':
    room_count = randint(10, 20)
    dungeon = Dungeon(opts.width, opts.height, room_count)
    dungeon.build_dungeon()