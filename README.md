# README #

This project contains python modules that generate a 2D dungeon in a command line environment.

### Generating the dungeon with default values ###
The following command generates a 2D gungeon of seed, width and height of 5, 50 and 60, respectively:  
python dungeon.py

### Generating the dungeon with user arguments ###
python dungeon.py -s [seed] -w [width] -t [height]  
[seed] allows you to generate the same dungeon each time.