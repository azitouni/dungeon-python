import unittest
from dungeon import Room, location, Dungeon

class TestDungeon(unittest.TestCase):
  def test_room_equality(self):
    room1 = Room(location(20, 40), 40, 50)
    room2 = Room(location(20, 40), 230, 300)
    self.assertEqual(room1, room2, "Rooms should be equal")
    self.assertTrue(room1 == room2, "Rooms should be equal")
    
  def test_equality_with_different_rooms(self):
    room1 = Room(location(20, 40), 40, 50)
    room2 = Room(location(20, 42), 230, 300)
    self.assertNotEqual(room1, room2, "Rooms shouldn't be equal")
    self.assertTrue(room1 != room2, "Rooms shouldn't be equal")
    
  def test_dungeon_rooms_collision(self):
    dungeon = Dungeon(100, 100, 20)
    room1 = Room(location(20, 40), 20, 30)
    room2 = Room(location(20, 42), 230, 300)
    room3 = Room(location(80, 80), 10, 15)
    dungeon.rooms.append(room1)
    self.assertTrue(dungeon.rooms_collide(room2), "room1 should collide with room2")
    self.assertFalse(dungeon.rooms_collide(room3), "room1 should collide with room3")
    
  def test_null_room_equality(self):
    room1 = Room(location(0, 0), 40, 50)
    room2 = Room.null_room()
    self.assertEqual(room1, room2, "room1 should be equal to the null room")
    
if __name__ == '__main__':
    unittest.main()